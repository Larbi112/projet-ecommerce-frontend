import { Component, OnInit } from '@angular/core';
import {CatalogueService} from '../catalogue.service';
import {Router, ActivatedRoute, Params, RoutesRecognized, NavigationEnd} from '@angular/router';
import {HttpEventType, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  private products;
  private editPhoto: boolean;
  private currentProduct: any;
  private selectedFile;
  private progress;
  private currentFileUpload;
  private title:string;
  constructor(private catService: CatalogueService,
              private route:ActivatedRoute,
              private router:Router){

//val est l'évènement
    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        let url=val.url;
        console.log(url);
        //console.log(val.state.root.firstChild.params);
      }
    });
  }

  ngOnInit(): void {

    this.router.events.subscribe(val => {
      if (val instanceof RoutesRecognized) {
        console.log(val.state.root.firstChild.params);
        let p1=val.id;
        console.log(p1);
        if(p1==1){
          this.title="sélection";
          this.getProducts('/products/search/selectedProducts/');
        }else if (p1==2) {
          let idCat=val.state.root.firstChild.params.p2;
          this.title="Produits de la catégorie"+idCat;
          this.getProducts('/categories/'+idCat+'/products')
        }
        else if (p1==3) {
          this.title="Produits en promotion"
          this.getProducts('/products/search/promoProducts/');
        }
        else if (p1==4) {
          this.title="Produits disponibles"
          this.getProducts('/products/search/dispoProducts/');
        }
        else if (p1==5) {
          this.title="Recherche ..."
          this.getProducts('/products/search/dispoProducts/');
        }
      }
    });

    //code qui marche pas :p1 est undefined d'ou l'utilisation de subscribe au dessus(regarde doc en ligne)
       //let p1=this.route.snapshot.params.p1;
      // if(p1==1){
       // this.getProducts('/products/search/selectedProducts/');
       //}else if (p1==2) {
      //let idCat=this.route.snapshot.params.p2;
      //this.getProducts('/categories'+idCat+'/products')
       //}
  }

  private getProducts(url) {
    this.catService.getRessouce(url)
      .subscribe(data => {
        this.products = data;
      }, error1 => {
        console.log(error1);
      });
  }

  onEditPhoto(p) {
    this.currentProduct=p;
    this.editPhoto=true;
  }

  onSelectedFile(event) {
this.selectedFile=event.target.files;  //contient l'ensemble des fichiers qui sont sélectionnés
  }

  uploadPhoto() {
    this.progress = 0;
    this.currentFileUpload = this.selectedFile.item(0);
    this.catService.uploadPhotoProduct(this.currentFileUpload, this.currentProduct.id).subscribe(event => {
      if ((event.type === HttpEventType.UploadProgress)) {
        this.progress = Math.round(100 * event.loaded / event.total)
      } else if (event instanceof HttpResponse) {
        alert("Téléchargement avec succés");
        this.getProducts('/products/search/selectedProducts/');
      }
    }, err => {
      alert("Problème de chargement");
    });
    this.selectedFile=undefined;
  }
}

